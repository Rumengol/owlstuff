
function writeOut(text){
    document.getElementById("ep").innerHTML += "\n" +   text
}

function getCurrentTab() {
    return browser.tabs.query({currentWindow: true, active: true});
}

function init(){
    

    getCurrentTab().then((tabs) => {
                try{
                let tab = tabs[0]

                let name = document.getElementById("lname");
                let link = document.getElementById("llink")
                let domain = document.getElementById("ldomain")
                let tags = document.getElementById("ltags")
                let desc = document.getElementById("ldesc")

                name.value = tab.title

                link.value = tab.url
                
                domain.value = tab.url.split("/")[2]
                
                document.getElementById("SaveOwlink").addEventListener("click", () => {
                    
                    browser.runtime.sendMessage({
                        name: name.value,
                        link: link.value, 
                        domain: domain.value,
                        tags: tags.value,
                        desc: desc.value
                    })
                
        })
    }
    catch(e){
        writeOut(e)
    }
    })    
}
document.addEventListener("DOMContentLoaded", init);

// browser.tabs.executeScript({file: "/downloader.js"}).then(onExecuted,onError)

// function onExecuted(result) {
//     writeOut(`We executed in all subframes`);
//   }
  
//   function onError(error) {
//     writeOut(`Error: ${error}`);
//   }
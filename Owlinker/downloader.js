(function() {
    function WriteAndSave(obj){
        let FileUrl = null

        let current = new Date();
        let cDate = current.getFullYear() + '-' + (current.getMonth() + 1) + '-' + current.getDate();
        let cTime = current.getHours() + ":" + current.getMinutes();
        let dateTime = cDate + ' ' + cTime;

        let text = `---
layout: post
title: ${obj.name.replace(":",",")}
date: ${dateTime}
link: ${obj.link}
domain: ${obj.domain}
domainlink: https://${obj.domain}
author: Rumengol
tags: [${obj.tags}]
---
${obj.desc}
`

        let cleanName = obj.name.toLowerCase().replace(/[ \\/:*?\"<>|]/g,"-").replace(/(-)\1+/g, '$1').normalize("NFD").replace(/\p{Diacritic}/gu, "")

        let filename = `${cDate}-${cleanName}.md`


        let fileData = new Blob([text], {type: 'text/plain'});

        if (FileUrl !== null) {
            window.URL.revokeObjectURL(FileUrl);
        }
        FileUrl = window.URL.createObjectURL(fileData);

        let downloading = browser.downloads.download({
            url:FileUrl,
            filename: filename,
            saveAs: true})

        downloading.then(onStartedDownload, onFailed);


}

browser.runtime.onMessage.addListener(WriteAndSave)

})();
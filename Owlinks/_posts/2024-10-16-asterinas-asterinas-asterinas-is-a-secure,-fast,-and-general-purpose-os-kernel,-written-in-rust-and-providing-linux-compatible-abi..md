---
        layout: post
        title: asterinas/asterinas, Asterinas is a secure, fast, and general-purpose OS kernel, written in Rust and providing Linux-compatible ABI.
        date: 2024-10-16 10:34
        link: https://github.com/asterinas/asterinas
        domain: github.com
        domainlink: https://github.com
        author: Rumengol
        tags: [OS,Asterinas,Rust]
        ---
        
        
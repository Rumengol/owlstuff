---
        layout: post
        title: The longevity bottleneck hypothesis, Could dinosaurs have shaped ageing in present‐day mammals? - BioEssays - 2023 - Magalhães - The longevity bottleneck hypothesis Could dinosaurs have shaped ageing in present‐day.pdf
        date: 2024-6-7 9:6
        link: https://onlinelibrary.wiley.com/doi/pdfdirect/10.1002/bies.202300098
        domain: onlinelibrary.wiley.com
        domainlink: https://onlinelibrary.wiley.com
        author: Rumengol
        tags: [Ageing,Senescence,Opinion Paper]
        ---
        
        
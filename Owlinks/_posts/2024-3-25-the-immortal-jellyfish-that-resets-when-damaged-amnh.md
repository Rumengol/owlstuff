---
        layout: post
        title: The "Immortal" Jellyfish That Resets When Damaged | AMNH
        date: 2024-3-25 10:51
        link: https://www.amnh.org/explore/news-blogs/on-exhibit-posts/the-immortal-jellyfish
        domain: www.amnh.org
        domainlink: https://www.amnh.org
        author: Rumengol
        tags: [Biology,Jellyfish,Immortality]
        ---
        
        
---
layout: post
title: Science Forum, Ten common statistical mistakes to watch out for when writing or reviewing a manuscript | eLife
date: 2023-9-25 11:5
link: https://elifesciences.org/articles/48175
domain: elifesciences.org
domainlink: https://elifesciences.org
author: Rumengol
tags: [Scientific Paper, Best Practices]
---


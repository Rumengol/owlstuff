---
layout: post
title: Conway's Game of Life is Omniperiodic
date: 2024-2-12 13:40
link: https://arxiv.org/pdf/2312.02799.pdf
domain: arxiv.org
domainlink: https://arxiv.org
author: Rumengol
tags: [Conway,Scientific Paper,Game of Life]
---


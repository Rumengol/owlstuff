---
        layout: post
        title: Testing CCC+TL Cosmology with Observed Baryon Acoustic Oscillation Features - IOPscience
        date: 2024-3-18 14:3
        link: https://iopscience.iop.org/article/10.3847/1538-4357/ad1bc6
        domain: iopscience.iop.org
        domainlink: https://iopscience.iop.org
        author: Rumengol
        tags: [Astrophysics]
        ---
        
        
---
layout: post
title: Which word begins with "y" and looks like an axe in this picture? - English Language & Usage Stack Exchange
date: 2024-1-18 9:4
link: https://english.stackexchange.com/questions/395382/which-word-begins-with-y-and-looks-like-an-axe-in-this-picture
domain: english.stackexchange.com
domainlink: https://english.stackexchange.com
author: Rumengol
tags: []
---
How do you spell "dedication"?

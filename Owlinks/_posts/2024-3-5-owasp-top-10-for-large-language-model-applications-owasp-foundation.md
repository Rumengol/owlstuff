---
        layout: post
        title: OWASP Top 10 for Large Language Model Applications | OWASP Foundation
        date: 2024-3-5 16:24
        link: https://owasp.org/www-project-top-10-for-large-language-model-applications/
        domain: owasp.org
        domainlink: https://owasp.org
        author: Rumengol
        tags: [LLM,AI,Cybersecurity]
        ---
        
        
---
layout: post
title: The first open source 3D atlas of human anatomy — Blender Conference 2022
date: 2024-2-7 15:39
link: https://conference.blender.org/2022/presentations/1365/
domain: conference.blender.org
domainlink: https://conference.blender.org
author: Rumengol
tags: [Blender,3D,Open Source]
---


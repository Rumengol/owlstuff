---
layout: post
title: Not just NVIDIA: GPU programming that runs everywhere
date: 2024-2-14 10:34
link: https://pythonspeed.com/articles/gpu-without-cuda/
domain: pythonspeed.com
domainlink: https://pythonspeed.com
author: Rumengol
tags: [Programming]
---


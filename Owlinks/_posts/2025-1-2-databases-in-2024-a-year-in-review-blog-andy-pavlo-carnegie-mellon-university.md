---
        layout: post
        title: Databases in 2024, A Year in Review // Blog // Andy Pavlo - Carnegie Mellon University
        date: 2025-1-2 10:14
        link: https://www.cs.cmu.edu/~pavlo/blog/2025/01/2024-databases-retrospective.html
        domain: www.cs.cmu.edu
        domainlink: https://www.cs.cmu.edu
        author: Rumengol
        tags: [Database,Review]
        ---
        
        
---
        layout: post
        title: popcar2/BadWebsiteBlocklist, A filter that blocks spam & misleading websites from appearing in search results via uBlocklist
        date: 2025-1-15 17:54
        link: https://github.com/popcar2/BadWebsiteBlocklist
        domain: github.com
        domainlink: https://github.com
        author: Rumengol
        tags: [Addon]
        ---
        
        
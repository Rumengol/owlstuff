---
        layout: post
        title: You can not simply publicly access private secure links, can you? | Vin01’s Blog
        date: 2024-3-8 11:3
        link: https://vin01.github.io/piptagole/security-tools/soar/urlscan/hybrid-analysis/data-leaks/urlscan.io/cloudflare-radar%22/2024/03/07/url-database-leaks-private-urls.html
        domain: vin01.github.io
        domainlink: https://vin01.github.io
        author: Rumengol
        tags: [Cybersecurity,Privacy]
        ---
        
        
---
        layout: post
        title: The Mystery of Numerical Notation on the Dial Plate - 4 is Expressed as IIII, not IV | THE SEIKO MUSEUM GINZA
        date: 2024-3-18 10:24
        link: https://museum.seiko.co.jp/en/knowledge/trivia02/
        domain: museum.seiko.co.jp
        domainlink: https://museum.seiko.co.jp
        author: Rumengol
        tags: [Time,Clock]
        ---
        
        
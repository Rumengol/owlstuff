---
layout: post
title: dessant/buster, Captcha solver extension for humans, available for Chrome, Edge and Firefox
date: 2024-1-22 20:30
link: https://github.com/dessant/buster
domain: github.com
domainlink: https://github.com
author: Rumengol
tags: [Privacy,Tool]
---


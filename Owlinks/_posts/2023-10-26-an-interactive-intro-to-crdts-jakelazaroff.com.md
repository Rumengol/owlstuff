---
layout: post
title: An Interactive Intro to CRDTs | jakelazaroff.com
date: 2023-10-26 11:8
link: https://jakelazaroff.com/words/an-interactive-intro-to-crdts/#user-content-fn-cvrdt
domain: jakelazaroff.com
domainlink: https://jakelazaroff.com
author: Rumengol
tags: []
---
Conflict-free Replicated Data Type, or how to sync data states

---
layout: post
title: How Hackerman would create an image just by typing zeros and ones — a deep dive into the GIF file format | by Oscar Olsson | Nov, 2023 | Medium
date: 2023-12-7 10:38
link: https://medium.com/@happybits/how-hackerman-would-create-an-image-just-by-typing-zeros-and-ones-a-deep-dive-into-gif-file-32bada0926c0
domain: medium.com
domainlink: https://medium.com
author: Rumengol
tags: [Gif]
---


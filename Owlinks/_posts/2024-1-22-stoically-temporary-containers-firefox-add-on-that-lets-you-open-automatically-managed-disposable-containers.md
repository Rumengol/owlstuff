---
layout: post
title: stoically/temporary-containers, Firefox Add-on that lets you open automatically managed disposable containers
date: 2024-1-22 20:6
link: https://github.com/stoically/temporary-containers
domain: github.com
domainlink: https://github.com
author: Rumengol
tags: [Privacy,Add-on]
---


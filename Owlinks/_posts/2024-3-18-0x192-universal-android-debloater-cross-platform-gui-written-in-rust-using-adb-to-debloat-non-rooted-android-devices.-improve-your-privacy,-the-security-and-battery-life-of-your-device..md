---
        layout: post
        title: 0x192/universal-android-debloater, Cross-platform GUI written in Rust using ADB to debloat non-rooted android devices. Improve your privacy, the security and battery life of your device.
        date: 2024-3-18 10:7
        link: https://github.com/0x192/universal-android-debloater
        domain: github.com
        domainlink: https://github.com
        author: Rumengol
        tags: [Privacy,Android]
        ---
        
        
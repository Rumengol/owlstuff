---
        layout: post
        title: GitHub - pingcap/autoflow, pingcap/autoflow is a Graph RAG based and conversational knowledge base tool built with TiDB Serverless Vector Storage. Demo: https://tidb.ai
        date: 2024-11-22 12:1
        link: https://github.com/pingcap/autoflow
        domain: github.com
        domainlink: https://github.com
        author: Rumengol
        tags: [Knowledge Graph,LLM,Open Source]
        ---
        
        
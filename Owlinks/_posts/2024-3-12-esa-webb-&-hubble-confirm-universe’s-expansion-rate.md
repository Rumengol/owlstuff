---
        layout: post
        title: ESA - Webb & Hubble confirm Universe’s expansion rate
        date: 2024-3-12 9:16
        link: https://www.esa.int/Science_Exploration/Space_Science/Webb/Webb_Hubble_confirm_Universe_s_expansion_rate
        domain: www.esa.int
        domainlink: https://www.esa.int
        author: Rumengol
        tags: [Cosmology]
        ---
        
        
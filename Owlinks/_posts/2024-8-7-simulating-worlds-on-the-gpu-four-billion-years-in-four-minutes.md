---
        layout: post
        title: Simulating worlds on the GPU, Four billion years in four minutes
        date: 2024-8-7 11:20
        link: https://davidar.io/post/sim-glsl
        domain: davidar.io
        domainlink: https://davidar.io
        author: Rumengol
        tags: [Programming,Simulation,Worldbuilding]
        ---
        
        
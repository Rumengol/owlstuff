---
        layout: post
        title: Average is Boring, How Similarity Kills a Meme's Success | Scientific Reports
        date: 2024-3-22 17:34
        link: https://www.nature.com/articles/srep06477
        domain: www.nature.com
        domainlink: https://www.nature.com
        author: Rumengol
        tags: [Memetics,Scientific Paper]
        ---
        
        
---
        layout: post
        title: Why choose async/await over threads? – notgull – The world's number one source of notgull
        date: 2024-3-25 10:39
        link: https://notgull.net/why-not-threads/
        domain: notgull.net
        domainlink: https://notgull.net
        author: Rumengol
        tags: [Programming,Rust,Async]
        ---
        
        
---
layout: post
title: Signal >> Blog >> Keep your phone number private with Signal usernames
date: 2024-2-21 10:11
link: https://signal.org/blog/phone-number-privacy-usernames/
domain: signal.org
domainlink: https://signal.org
author: Rumengol
tags: [Privacy]
---


---
        layout: post
        title: How an empty S3 bucket can make your AWS bill explode | by Maciej Pocwierz | Apr, 2024 | Medium
        date: 2024-5-2 11:3
        link: https://medium.com/@maciej.pocwierz/how-an-empty-s3-bucket-can-make-your-aws-bill-explode-934a383cb8b1
        domain: medium.com
        domainlink: https://medium.com
        author: Rumengol
        tags: [AWS,Hosting]
        ---
        
        
---
layout: post
title: Patient-specific analysis of co-expression to measure biological network rewiring in individuals | Life Science Alliance
date: 2024-2-2 14:57
link: https://www.life-science-alliance.org/content/7/2/e202302253
domain: www.life-science-alliance.org
domainlink: https://www.life-science-alliance.org
author: Rumengol
tags: [Scientific Paper,Co-Expression]
---


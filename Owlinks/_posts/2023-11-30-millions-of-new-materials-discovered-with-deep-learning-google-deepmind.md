---
layout: post
title: Millions of new materials discovered with deep learning - Google DeepMind
date: 2023-11-30 16:1
link: https://deepmind.google/discover/blog/millions-of-new-materials-discovered-with-deep-learning/
domain: deepmind.google
domainlink: https://deepmind.google
author: Rumengol
tags: [Machine Learning]
---


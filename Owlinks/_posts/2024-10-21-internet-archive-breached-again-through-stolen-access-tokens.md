---
        layout: post
        title: Internet Archive breached again through stolen access tokens
        date: 2024-10-21 10:38
        link: https://www.bleepingcomputer.com/news/security/internet-archive-breached-again-through-stolen-access-tokens/
        domain: www.bleepingcomputer.com
        domainlink: https://www.bleepingcomputer.com
        author: Rumengol
        tags: [Internet Archive,Cybersecurity]
        ---
        
        
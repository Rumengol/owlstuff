---
        layout: post
        title: Considerations for a long-running Raspberry Pi # Chris Dzombak
        date: 2024-2-19 9:37
        link: https://www.dzombak.com/blog/2023/12/Considerations-for-a-long-running-Raspberry-Pi.html
        domain: www.dzombak.com
        domainlink: https://www.dzombak.com
        author: Rumengol
        tags: [Raspberry Pi]
        ---
        
        
---
        layout: post
        title: Jeremy Couillard’s video games capture what it’s like to be alive right now | Art Basel
        date: 2024-9-11 14:31
        link: https://www.artbasel.com/stories/jeremy-couillard-video-games-mit-list-visual-arts-center-critique-contemporary-life-digital-landscapes
        domain: www.artbasel.com
        domainlink: https://www.artbasel.com
        author: Rumengol
        tags: [Further Reading]
        ---
        
        
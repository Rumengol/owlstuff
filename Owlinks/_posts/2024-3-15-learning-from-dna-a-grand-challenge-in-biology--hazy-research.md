---
        layout: post
        title: Learning from DNA, a grand challenge in biology · Hazy Research
        date: 2024-3-15 9:18
        link: https://hazyresearch.stanford.edu/blog/2024-03-14-evo
        domain: hazyresearch.stanford.edu
        domainlink: https://hazyresearch.stanford.edu
        author: Rumengol
        tags: [AI,LLM,DNA,Genomics,Generative AI]
        ---
        
        
---
        layout: post
        title: Meet Willow, our state-of-the-art quantum chip
        date: 2024-12-10 8:55
        link: https://blog.google/technology/research/google-willow-quantum-chip/
        domain: blog.google
        domainlink: https://blog.google
        author: Rumengol
        tags: [Quantum Computing]
        ---
        
        
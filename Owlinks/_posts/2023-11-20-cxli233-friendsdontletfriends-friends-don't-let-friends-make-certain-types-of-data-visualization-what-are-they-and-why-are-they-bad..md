---
layout: post
title: Friends don't let friends make certain types of data visualization - What are they and why are they bad.
date: 2023-11-20 13:48
link: https://github.com/cxli233/FriendsDontLetFriends
domain: github.com
domainlink: https://github.com
author: Rumengol
tags: [Data Visualisation, Best Practices]
---
A collection on advice to make plots more meaningful

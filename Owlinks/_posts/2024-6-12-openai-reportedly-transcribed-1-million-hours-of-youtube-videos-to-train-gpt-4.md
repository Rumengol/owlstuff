---
        layout: post
        title: OpenAI Reportedly Transcribed 1 Million Hours of YouTube Videos to Train GPT-4
        date: 2024-6-12 14:59
        link: https://gizmodo.com/openai-chatgpt-google-youtube-videos-train-gpt4-1851394676
        domain: gizmodo.com
        domainlink: https://gizmodo.com
        author: Rumengol
        tags: [AI,AI Detector]
        ---
        
        
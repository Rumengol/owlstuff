---
layout: post
title: GALA3D: Towards Text-to-3D Complex Scene Generation via Layout-guidedGenerative Gaussian Splatting
date: 2024-2-20 10:3
link: https://gala3d.github.io/
domain: gala3d.github.io
domainlink: https://gala3d.github.io
author: Rumengol
tags: [AI,LLM,Generative AI,3D]
---


---
layout: post
title: dessant/web-archives, Browser extension for viewing archived and cached versions of web pages, available for Chrome, Edge and Safari
date: 2024-1-22 20:06
link: https://github.com/dessant/web-archives
domain: github.com
domainlink: https://github.com
author: Rumengol
tags: [Add-on,Archive]
---


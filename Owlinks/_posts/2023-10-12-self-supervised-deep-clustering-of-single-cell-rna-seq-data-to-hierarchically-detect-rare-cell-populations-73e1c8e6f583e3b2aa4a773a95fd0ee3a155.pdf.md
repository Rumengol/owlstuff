---
layout: post
title: Self-supervised deep clustering of single-cell RNA-seq data to hierarchically detect rare cell populations - 73e1c8e6f583e3b2aa4a773a95fd0ee3a155.pdf
date: 2023-10-12 10:30
link: https://pdfs.semanticscholar.org/0e24/73e1c8e6f583e3b2aa4a773a95fd0ee3a155.pdf
domain: pdfs.semanticscholar.org
domainlink: https://pdfs.semanticscholar.org
author: Rumengol
tags: [Single Cell, Scientific Paper]
---
A clustering method based on auto-encoder and self supervision, could be interesting to try (implemented in python)

---
layout: post
title: Making Rust binaries smaller by default | Kobzol’s blog
date: 2024-1-25 10:14
link: https://kobzol.github.io/rust/cargo/2024/01/23/making-rust-binaries-smaller-by-default.html
domain: kobzol.github.io
domainlink: https://kobzol.github.io
author: Rumengol
tags: [Rust]
---


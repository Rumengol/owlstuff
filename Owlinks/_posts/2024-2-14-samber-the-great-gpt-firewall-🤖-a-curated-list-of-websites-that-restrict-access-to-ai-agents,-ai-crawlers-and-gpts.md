---
layout: post
title: samber/the-great-gpt-firewall: 🤖 A curated list of websites that restrict access to AI Agents, AI crawlers and GPTs
date: 2024-2-14 10:29
link: https://github.com/samber/the-great-gpt-firewall
domain: github.com
domainlink: https://github.com
author: Rumengol
tags: [Collection,LLM,Crawler,Internet]
---


---
        layout: post
        title: graphcore-research/kg-topology-toolbox, A Python toolbox to compute topological metrics and statistics for Knowledge Graphs
        date: 2024-9-11 11:38
        link: https://github.com/graphcore-research/kg-topology-toolbox
        domain: github.com
        domainlink: https://github.com
        author: Rumengol
        tags: [Knowledge Graph,Topology,Graph Analysis]
        ---
        
        
---
layout: post
title: Perplexity
date: 2023-11-21 16:13
link: https://www.perplexity.ai/
domain: www.perplexity.ai
domainlink: https://www.perplexity.ai
author: Rumengol
tags: [AI,Tool]
---
Un outil de conversation vers ChatGPT pouvant utiliser Copilot et lire des PDF, ainsi que cibler ses recherches.

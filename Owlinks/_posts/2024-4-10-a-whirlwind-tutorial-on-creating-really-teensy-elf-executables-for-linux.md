---
        layout: post
        title: A Whirlwind Tutorial on Creating Really Teensy ELF Executables for Linux
        date: 2024-4-10 8:30
        link: https://www.muppetlabs.com/~breadbox/software/tiny/teensy.html
        domain: www.muppetlabs.com
        domainlink: https://www.muppetlabs.com
        author: Rumengol
        tags: [ELF,Linux,Programming,C,Assembly]
        ---
        
        
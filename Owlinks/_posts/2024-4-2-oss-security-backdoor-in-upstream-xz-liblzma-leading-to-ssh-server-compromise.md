---
        layout: post
        title: oss-security - backdoor in upstream xz/liblzma leading to ssh server compromise
        date: 2024-4-2 9:58
        link: https://www.openwall.com/lists/oss-security/2024/03/29/4
        domain: www.openwall.com
        domainlink: https://www.openwall.com
        author: Rumengol
        tags: [XZ,Backdoor,Cybersecurity]
        ---
        
        
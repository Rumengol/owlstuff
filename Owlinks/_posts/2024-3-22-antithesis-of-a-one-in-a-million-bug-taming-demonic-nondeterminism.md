---
        layout: post
        title: Antithesis of a One-in-a-Million Bug, Taming Demonic Nondeterminism
        date: 2024-3-22 17:24
        link: https://www.cockroachlabs.com/blog/demonic-nondeterminism/
        domain: www.cockroachlabs.com
        domainlink: https://www.cockroachlabs.com
        author: Rumengol
        tags: [Programming,Determinism]
        ---
        
        
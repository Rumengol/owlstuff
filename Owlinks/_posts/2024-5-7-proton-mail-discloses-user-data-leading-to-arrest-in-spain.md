---
        layout: post
        title: Proton Mail Discloses User Data Leading to Arrest in Spain
        date: 2024-5-7 11:15
        link: https://restoreprivacy.com/protonmail-discloses-user-data-leading-to-arrest-in-spain/
        domain: restoreprivacy.com
        domainlink: https://restoreprivacy.com
        author: Rumengol
        tags: [Privacy,Proton]
        ---
        
        
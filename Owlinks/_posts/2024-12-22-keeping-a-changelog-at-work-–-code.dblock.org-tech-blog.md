---
        layout: post
        title: Keeping a CHANGELOG at Work – code.dblock.org | tech blog
        date: 2024-12-22 11:46
        link: https://code.dblock.org/2020/09/01/keep-a-changelog-at-work.html
        domain: code.dblock.org
        domainlink: https://code.dblock.org
        author: Rumengol
        tags: [Changelog,Best Practice,Productivity]
        ---
        
        
---
        layout: post
        title: praetorian-inc/noseyparker, Nosey Parker is a command-line program that finds secrets and sensitive information in textual data and Git history.
        date: 2024-3-5 11:58
        link: https://github.com/praetorian-inc/noseyparker/
        domain: github.com
        domainlink: https://github.com
        author: Rumengol
        tags: [Cybersecurity]
        ---
        
        
---
        layout: post
        title: Tiny beauty, how I make scientific art from behind the microscope
        date: 2024-6-19 15:7
        link: https://www.nature.com/immersive/d41586-024-02011-6/index.html
        domain: www.nature.com
        domainlink: https://www.nature.com
        author: Rumengol
        tags: [Picture,Science,Microscopy]
        ---
        
        
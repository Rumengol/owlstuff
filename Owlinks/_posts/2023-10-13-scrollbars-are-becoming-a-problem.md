---
layout: post
title: Scrollbars are becoming a problem
date: 2023-10-13 16:5
link: https://artemis.sh/2023/10/12/scrollbars.html
domain: artemis.sh
domainlink: https://artemis.sh
author: Rumengol
tags: [Accessibility]
---
How to get back scrollbars, at least in firefox

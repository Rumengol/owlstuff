---
        layout: post
        title: Maximum Likelihood Estimation and Loss Functions | Rish's AI Notes
        date: 2024-12-16 13:12
        link: https://rish-01.github.io/blog/posts/ml_estimation/
        domain: rish-01.github.io
        domainlink: https://rish-01.github.io
        author: Rumengol
        tags: [Loss,Machine Learning,Math]
        ---
        
        
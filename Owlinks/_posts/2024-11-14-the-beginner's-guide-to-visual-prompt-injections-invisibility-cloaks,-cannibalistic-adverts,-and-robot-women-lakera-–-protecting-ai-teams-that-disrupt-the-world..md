---
        layout: post
        title: The Beginner's Guide to Visual Prompt Injections, Invisibility Cloaks, Cannibalistic Adverts, and Robot Women | Lakera – Protecting AI teams that disrupt the world.
        date: 2024-11-14 14:10
        link: https://www.lakera.ai/blog/visual-prompt-injections
        domain: www.lakera.ai
        domainlink: https://www.lakera.ai
        author: Rumengol
        tags: [LLM,Visual Prompt Injection]
        ---
        
        
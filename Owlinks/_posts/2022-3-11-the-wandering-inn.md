---
layout: post
title: The Wandering Inn
date: 2022-3-11 23:8
link: https://wanderinginn.com/
domain: wanderinginn.com
domainlink: https://wanderinginn.com
author: Rumengol
tags: [Litterature,Webnovel,Fantasy,Isekai]
---
Webnovel de plusieurs tomes et quelques millions de mots, à base d'une fille se faisant isekai'd dans une auberge.

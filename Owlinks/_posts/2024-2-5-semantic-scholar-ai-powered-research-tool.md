---
layout: post
title: Semantic Scholar | AI-Powered Research Tool
date: 2024-2-5 15:20
link: https://www.semanticscholar.org/
domain: www.semanticscholar.org
domainlink: https://www.semanticscholar.org
author: Rumengol
tags: [Favorite,Search Engine,AI,Bibliography]
---
Awesome 3-in-1 solution to keep a bibliography, search for new articles and keep up to date with the state of the art.

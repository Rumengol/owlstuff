---
layout: post
title: About
permalink: /about/
author: Rumengol
---

Inspired by [Les liens de Corentin](https://links.l3m.in/fr/), I realized that if I am going to keep over 5000 tabs open (thanks to [New Tab Suspender](https://github.com/pradeep-mishra/tab_suspender_firefox) and [Simple Tab Groups](https://github.com/drive4ik/simple-tab-groups)), I might as well save the most important ones as bookmarks. And then, why not sharing them online? I can't be the only one interested in the countless things I come across every day!

The code of both this website and the custom extension that gather the bookmarks is available [on Gitlab](https://gitlab.com/Rumengol/owlstuff).
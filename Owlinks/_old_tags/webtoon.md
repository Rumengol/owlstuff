---
name: Webtoon
---

L'art de la bande-dessinée digitale venue de Corée du Sud, qui se lit à la vertical sur un écran.
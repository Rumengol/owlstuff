---
permalink: /404.html
layout: default
---

<style type="text/css" media="screen">
  .container {
    margin: 10px auto;
    max-width: 600px;
    text-align: center;
  }
  h1 {
    margin: 30px 0;
    font-size: 4em;
    line-height: 1;
    letter-spacing: -1px;
  }
</style>

<div class="container">
  <h1>404</h1>

  <p><strong>Page introuvable</strong></p>
  <p>J'ai beau avoir cherché partout, cette page a dû s'égarer. A la place, j'ai trouvé ça :</p>
</div>

{% assign random = site.time | date: "%s%N" | plus : 10 | modulo: site.posts.size %}
{% assign post = site.posts[random] %}
{% include catalogue_item.html %}


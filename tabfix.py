import os
posts = os.path.join(os.getcwd(),"Owlinks/_posts")
tags = os.path.join(os.getcwd(),"Owlinks/_tags")

if not os.path.exists(tags): os.mkdir(tags)

all_tags = []

for t in os.listdir(tags):
        all_tags.append(os.path.splitext(t)[0])

def create_tag(content):
        taglist = "\n".join(content).split("tags: [")[1].split("]")[0].split(",")
        for tag in taglist:
                clean_tag = tag.strip().replace(" ","_").replace("/","-")
                if len(tag)>0 and clean_tag not in all_tags:
                        print(f"Creating {clean_tag}.md")
                        with open(os.path.join(tags,clean_tag +".md"),"w") as fi:
                            fi.write(f"---\nname: {tag}\n---\n")
                        all_tags.append(clean_tag)
                            

for f in os.listdir(posts):
    f = os.path.join(posts,f)
    with open(f,"r", encoding="utf-8") as fi:
            try:
                content = fi.readlines()
            except Exception as e:
                   print(f)
                   raise e
            create_tag(content)
    with open(f,"w", encoding="utf-8") as fi:
            content[2] = "title: \"" + content[2].split("title: ")[1].replace('"','\\"').rstrip() + '"\n'
            fi.write("".join([line.replace("        ","") for line in content]))